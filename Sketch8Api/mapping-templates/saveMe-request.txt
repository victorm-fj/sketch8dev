{
  "version": "2017-02-28",
  "operation": "PutItem",
  "key": {
    "userId": { "S": "${context.identity.cognitoIdentityId}" }
  },
  #set($attrs = $util.dynamodb.toMapValues($context.arguments))
  #set($createdAt = $util.time.nowEpochMilliSeconds())
  #set($attrs.createdAt = $util.dynamodb.toNumber($createdAt))
  #set($attrs.coins = $util.dynamodb.toNumber(0))
  "attributeValues": $util.toJson($attrs)
}