/**
 * @format
 * @flow
 */

import React from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Button } from 'react-native-elements';
import { iOSUIKit } from 'react-native-typography';
import { Transition } from 'react-navigation-fluid-transitions';

import styles from './styles';

const SERVER_URL = 'http://zilln.com';

type Item = {
	_id: string,
	name: string,
	price: string,
	description: string,
	images: Array<string>,
};
type Props = { onPress?: Function, item: Item, onPressGet?: Function };
const ProductItem = (props: Props) => {
	const {
		onPress,
		onPressGet,
		item: { images, name, price, _id },
	} = props;
	return (
		<View style={styles.shadow}>
			<View style={styles.container}>
				<TouchableWithoutFeedback onPress={onPress}>
					<View style={styles.cardContent}>
						<Transition shared={`image${_id}`}>
							<FastImage
								source={{ uri: `${images[0].url}` }}
								resizeMode={FastImage.resizeMode.stretch}
								style={styles.image}
							/>
						</Transition>

						<View style={styles.footerContainer}>
							<Transition shared={`footer${_id}`}>
								<View style={styles.footerBackground} />
							</Transition>

							<View style={styles.rowContainer}>
								<View style={{ flex: 1 }}>
									<Transition shared={`footerTitle${_id}`}>
										<Text style={[iOSUIKit.subheadEmphasized, styles.footerTitle]}>{name}</Text>
									</Transition>

									<Transition shared={`footerSubtitle${_id}`}>
										<Text style={[iOSUIKit.footnote, styles.footerSubtitle]}>$ {price}</Text>
									</Transition>
								</View>

								<Transition shared={`footerAction${_id}`}>
									<Button
										title="GET"
										titleStyle={styles.footerButtonTitle}
										buttonStyle={styles.footerButton}
										onPress={onPressGet}
										containerStyle={{ marginTop: 3 }}
									/>
								</Transition>
							</View>
						</View>
					</View>
				</TouchableWithoutFeedback>
			</View>
		</View>
	);
};
ProductItem.defaultProps = {
	onPress: () => {},
	onPressGet: () => {},
};

export default ProductItem;
