/**
 * @format
 * @flow
 */
import * as React from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { iOSUIKit, iOSColors } from 'react-native-typography';
import { Button } from 'react-native-elements';
import { Transition } from 'react-navigation-fluid-transitions';
import FastImage from 'react-native-fast-image';

import soccergame from './soccergame.jpg';

import styles from './styles';

type Props = {
	footer?: {
		title: string,
		subtitle: string,
		backgroundColor: string,
		textColor: string,
	},
	hideFooter?: boolean,
	onPress?: Function,
	id?: string,
	imageText?: string,
	imageTextColor?: string,
	imageTextPosition?: string,
	image?: any,
	coins?: number,
	onPressSolve?: Function,
};
class Card extends React.Component<Props> {
	static defaultProps = {
		footer: {
			title: 'Fiete soccer',
			subtitle: 'Soccer for the children',
			backgroundColor: iOSColors.purple,
			textColor: iOSColors.white,
		},
		hideFooter: false,
		onPress: () => {},
		id: 'card1',
		imageText: 'APP\nOF THE\nDAY',
		imageTextColor: 'black',
		imageTextPosition: 'bottom',
		image: soccergame,
		coins: 5.55,
		onPressSolve: () => {},
	};

	renderFooter() {
		const { id, hideFooter, footer, onPressSolve } = this.props;
		if (!hideFooter) {
			return (
				<View style={styles.footerContainer}>
					<Transition shared={`footer${id}`}>
						<View
							style={[
								styles.footerBackground,
								{
									backgroundColor: footer.backgroundColor,
								},
							]}
						/>
					</Transition>

					<View style={styles.rowContainer}>
						<View style={{ flex: 1 }}>
							<Transition shared={`footerActionTitle${id}`}>
								<Text
									style={[
										iOSUIKit.subheadEmphasized,
										styles.footerActionTitle,
										{
											color: footer.textColor,
										},
									]}
								>
									{footer.title}
								</Text>
							</Transition>
							<Transition shared={`footerActionSubtitle${id}`}>
								<Text
									style={[
										iOSUIKit.footnote,
										styles.footerActionSubtitle,
										{
											color: footer.textColor,
										},
									]}
								>
									{footer.subtitle}
								</Text>
							</Transition>
						</View>
						<Transition shared={`footerAction${id}`}>
							<Button
								title="SOLVE"
								titleStyle={styles.footerButtonTitle}
								buttonStyle={styles.footerButton}
								onPress={onPressSolve}
								containerStyle={{ marginTop: 3 }}
							/>
						</Transition>
					</View>
				</View>
			);
		}
		return null;
	}

	render() {
		const { id, onPress, imageText, image, imageTextColor, imageTextPosition, coins } = this.props;
		return (
			<View style={styles.shadow}>
				<View style={styles.container}>
					<TouchableWithoutFeedback onPress={() => onPress(id)}>
						<View style={styles.cardContent}>
							<Transition shared={`${id}`}>
								<FastImage
									source={image}
									resizeMode={FastImage.resizeMode.stretch}
									style={styles.image}
								/>
							</Transition>
							<Transition shared={`title${id}`}>
								<Text
									style={[
										styles.imageText,
										{
											color: imageTextColor,
											[imageTextPosition]: imageTextPosition === 'bottom' ? 80 : 20,
										},
									]}
								>
									{imageText}
								</Text>
							</Transition>
							{this.renderFooter()}
						</View>
					</TouchableWithoutFeedback>
				</View>
				<Text style={[iOSUIKit.subheadEmphasized, { color: iOSColors.blue, textAlign: 'center' }]}>
					Get &pound;
					{coins} now
				</Text>
			</View>
		);
	}
}

export default Card;
