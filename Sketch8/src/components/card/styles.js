import { StyleSheet, Platform } from 'react-native';
import { iOSColors } from 'react-native-typography';

export default StyleSheet.create({
	container: {
		width: '100%',
		overflow: 'hidden',
		marginVertical: 15,
		backgroundColor: 'transparent',
		borderRadius: 15,
		height: 415,
	},
	shadow: {
		...Platform.select({
			ios: {
				shadowColor: 'black',
				shadowOpacity: 0.3,
				shadowRadius: 15,
				shadowOffset: {
					width: 0,
					height: 15,
				},
			},
			android: {
				elevation: 6,
			},
		}),
	},
	footerContainer: {
		width: '100%',
		paddingHorizontal: 20,
		paddingVertical: 15,
		height: 70,
		position: 'absolute',
		left: 0,
		bottom: 0,
	},
	rowContainer: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	footerButton: {
		backgroundColor: iOSColors.white,
		borderRadius: 30,
		paddingHorizontal: 10,
	},
	footerButtonTitle: {
		color: iOSColors.blue,
		fontWeight: 'bold',
		fontSize: 15,
	},
	image: {
		flex: 1,
		width: '100%',
	},
	cardContent: {
		width: '100%',
		height: '100%',
		borderRadius: 15,
	},
	imageText: {
		fontSize: 40,
		fontWeight: 'bold',
		color: 'white',
		position: 'absolute',
		left: 20,
		// bottom: 80,
		width: 150,
		lineHeight: 40,
	},
	footerBackground: {
		height: 70,
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
	},
	footerActionTitle: {
		position: 'absolute',
		top: -18,
		left: 0,
	},
	footerActionSubtitle: {
		position: 'absolute',
		top: 5,
		left: 0,
	},
});
