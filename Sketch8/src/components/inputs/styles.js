import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	inputContainer: {
		width: 220,
		marginBottom: 24,
	},
	phoneInputContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 300,
		backgroundColor: 'transparent',
	},
});
