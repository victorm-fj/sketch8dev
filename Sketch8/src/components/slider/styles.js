import { StyleSheet } from 'react-native';

const colors = {
	white: '#f9fafc',
	black: '#3b4859',
	gray: '#8392a7',
	purple: 'purple',
};

export default StyleSheet.create({
	sliderContainer: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: colors.white,
		width: '100%',
	},
	slideContainer: {
		flex: 1,
		width: '100%',
		backgroundColor: colors.white,
	},
	mediaContainer: {
		flex: 0.66,
		width: '100%',
	},
	textContainer: {
		flex: 0.33,
		width: '100%',
		paddingHorizontal: 10,
		backgroundColor: colors.white,
	},
	titleText: {
		fontSize: 24,
		color: colors.black,
		marginVertical: 20,
		textAlign: 'center',
	},
	bodyText: {
		fontSize: 14,
		color: colors.gray,
		textAlign: 'center',
		lineHeight: 18,
	},
});
