// @flow
import * as React from 'react';
import { View, Dimensions, Platform } from 'react-native';
import Swiper from 'react-native-swiper';

import styles from './styles';

const { width } = Dimensions.get('window');

type Props = { children: React.Node, updateIndex?: Function };
class Slider extends React.Component<Props> {
	static defaultProps = { updateIndex: () => {} };

	swiper: ?React.Ref<typeof React.Component>;

	scrollBy(index: number) {
		if (this.swiper) {
			this.swiper.scrollBy(index);
		}
	}

	render() {
		const { children, updateIndex, ...rest } = this.props;
		const swiperStyle = Platform.OS === 'android' ? { width } : {};
		return (
			<View style={styles.sliderContainer}>
				<Swiper
					style={swiperStyle}
					removeClippedSubviews={false}
					ref={(swiper) => (this.swiper = swiper)}
					showsButtons={false}
					paginationStyle={{ position: 'absolute', bottom: 20 }}
					doStyle={{ marginHorizontal: 4 }}
					dotColor="#8392a7"
					activeDotColor="#3b4859"
					onIndexChanged={updateIndex}
					loop={false}
					{...rest}
				>
					{children}
				</Swiper>
			</View>
		);
	}
}

export default Slider;
