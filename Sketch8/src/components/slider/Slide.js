/**
 * @format
 * @flow
 */
import * as React from 'react';
import { View, Text, ViewPropTypes } from 'react-native';

import styles from './styles';

type Props = {
	style?: ViewPropTypes.style,
	data: { id: number, title: string, body: string },
	children?: React.Node,
};
const Slide = ({ style, data: { title, body }, children }: Props) => (
	<View style={[styles.slideContainer, style]}>
		<View style={styles.mediaContainer}>{children}</View>
		<View style={styles.textContainer}>
			<Text style={styles.titleText}>{title}</Text>
			<Text style={styles.bodyText}>{body}</Text>
		</View>
	</View>
);
Slide.defaultProps = {
	style: {},
	children: null,
};

export default Slide;
