import AWSAppSyncClient from 'aws-appsync';
import { Auth } from 'aws-amplify';

import awsmobile from './aws-exports';

const client = new AWSAppSyncClient({
	url: awsmobile.aws_appsync_graphqlEndpoint,
	region: awsmobile.aws_appsync_region,
	auth: {
		type: awsmobile.aws_appsync_authenticationType,
		// IAM
		credentials: () => Auth.currentCredentials(),
	},
	complexObjectsCredentials: () => Auth.currentCredentials(),
	disableOffline: true,
});

export default client;
