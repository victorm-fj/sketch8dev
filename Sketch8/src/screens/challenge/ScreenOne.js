import React from 'react';
import { View, Text } from 'react-native';

import styles from '../../styles';

const ScreenOne = () => (
	<View style={styles.screenContainer}>
		<Text>Challenge. Screen One</Text>
	</View>
);

export default ScreenOne;
