import React from 'react';
import { View, Text } from 'react-native';

import styles from '../../styles';

const ScreenTwo = () => (
	<View style={styles.screenContainer}>
		<Text>Challenge. Screen Two</Text>
	</View>
);

export default ScreenTwo;
