/**
 * @format
 * @flow
 */
import React from 'react';
import { View, Text, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import { graphql } from 'react-apollo';
import type { NavigationScreenProp } from 'react-navigation';

import { UpdateMe } from '../../graphQL';
import styles from '../../styles';

type Props = { navigation: NavigationScreenProp<*>, onUpdateMe: Function };
class ScreenThree extends React.Component<Props> {
	onPressHandler = async () => {
		const { onUpdateMe, navigation } = this.props;
		const strCoins = await AsyncStorage.getItem('@challenge:coins');
		const coins = strCoins || '5.55';
		onUpdateMe({ coins });
		navigation.navigate('Today');
	};

	render() {
		return (
			<View style={styles.screenContainer}>
				<Text style={{ marginBottom: 20 }}>Challenge. Screen Three</Text>
				<Button title="Complete challenge" onPress={this.onPressHandler} />
			</View>
		);
	}
}

export default graphql(UpdateMe, {
	props: ({ mutate }) => ({
		onUpdateMe: info => mutate({ variables: { info } }),
	}),
})(ScreenThree);
