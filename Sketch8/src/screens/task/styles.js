import { StyleSheet, Platform } from 'react-native';
import { iOSColors } from 'react-native-typography';

export default StyleSheet.create({
	footerContainer: {
		width: '100%',
		paddingHorizontal: 20,
		paddingVertical: 15,
		height: 70,
		position: 'absolute',
		left: 0,
		bottom: 0,
	},
	rowContainer: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	buttonTitleStyle: {
		color: iOSColors.blue,
		fontWeight: 'bold',
		fontSize: 15,
	},
	buttonStyle: {
		backgroundColor: iOSColors.white,
		borderRadius: 30,
		paddingHorizontal: 10,
	},
	iconContainer: {
		position: 'absolute',
		top: 15,
		right: 15,
		backgroundColor: 'rgba(0,0,0,0.5)',
		width: 28,
		height: 28,
		borderRadius: 14,
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: 999,
	},
	challengeTitleStyle: {
		fontSize: 40,
		fontWeight: 'bold',
		color: 'white',
		position: 'absolute',
		left: 20,
		bottom: 80,
		width: 150,
		lineHeight: 40,
	},
	challengeImageStyle: {
		flex: 1,
		width: '100%',
	},
	shadow: {
		...Platform.select({
			ios: {
				shadowColor: 'black',
				shadowOpacity: 0.3,
				shadowRadius: 15,
				shadowOffset: {
					width: 0,
					height: 15,
				},
			},
			android: {
				elevation: 6,
			},
		}),
	},
});
