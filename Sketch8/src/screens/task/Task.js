/**
 * @format
 * @flow
 */
import React from 'react';
import {
	ScrollView,
	View,
	StatusBar,
	Text,
	Dimensions,
	Animated,
	TouchableOpacity,
} from 'react-native';
import { Transition } from 'react-navigation-fluid-transitions';
import { Button } from 'react-native-elements';
import type { NavigationScreenProp } from 'react-navigation';
import { iOSUIKit, iOSColors } from 'react-native-typography';
import Markdown from 'react-native-markdown-renderer';
import FastImage from 'react-native-fast-image';
import Ionicons from 'react-native-vector-icons/Ionicons';

import styles from '../../styles';
import taskStyles from './styles';

const { height } = Dimensions.get('window');

const contentAppearTransition = transitionInfo => {
	const { progress, start, end, boundingbox, direction } = transitionInfo;

	const distanceValue = -(boundingbox.height + boundingbox.y + 25);
	let startValue = 0;
	let endValue = distanceValue;

	if (direction === 2) {
		startValue = distanceValue;
		endValue = 0;
	}

	const translateY = progress.interpolate({
		inputRange: [0, start, end, 1],
		outputRange: [startValue, startValue, endValue, endValue],
	});

	const opacity = progress.interpolate({
		inputRange: [0, start, end, 1],
		outputRange: [0, 0, 0.05, 1],
	});

	return {
		transform: [
			{
				translateY,
			},
		],
		opacity,
	};
};

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

type Props = { navigation: NavigationScreenProp<*> };
class Task extends React.Component<Props> {
	scrollY = new Animated.Value(0);

	onPressHandler = () => {
		const { navigation } = this.props;
		navigation.goBack();
	};

	onPressSolveHandler = () => {
		const { navigation } = this.props;
		navigation.navigate('ScreenOne');
	};

	onMomentumScrollBeginHandler = (event: Object) => {
		const {
			nativeEvent: {
				contentOffset: { y },
			},
		} = event;
		if (y < -40) {
			this.onPressHandler();
		}
	};

	renderFooter() {
		const { navigation } = this.props;
		const {
			state: {
				params: { challenge },
			},
		} = navigation;
		return (
			<View style={taskStyles.footerContainer}>
				<Transition shared={`footer${challenge._id}`}>
					<View
						style={[
							{
								height: 70,
								backgroundColor: challenge.actionBackgroundColor,
								position: 'absolute',
								top: 0,
								left: 0,
								bottom: 0,
								right: 0,
							},
						]}
					/>
				</Transition>
				<View style={taskStyles.rowContainer}>
					<View style={{ flex: 1 }}>
						<Transition shared={`footerActionTitle${challenge._id}`}>
							<Text
								style={[
									iOSUIKit.subheadEmphasized,
									{
										color: challenge.actionTextColor,
										position: 'absolute',
										top: -18,
										left: 0,
									},
								]}
							>
								{challenge.actionTitle || 'Action title'}
							</Text>
						</Transition>
						<Transition shared={`footerActionSubtitle${challenge._id}`}>
							<Text
								style={[
									iOSUIKit.footnote,
									{
										color: challenge.actionTextColor,
										position: 'absolute',
										top: 5,
										left: 0,
									},
								]}
							>
								{challenge.actionSubtitle || 'Action subtitle'}
							</Text>
						</Transition>
					</View>
					<Transition shared={`footerAction${challenge._id}`}>
						<Button
							title="SOLVE"
							titleStyle={taskStyles.buttonTitleStyle}
							buttonStyle={taskStyles.buttonStyle}
							onPress={this.onPressSolveHandler}
							containerStyle={{ marginTop: 3 }}
						/>
					</Transition>
				</View>
			</View>
		);
	}

	render() {
		const {
			navigation: {
				state: {
					params: { challenge },
				},
			},
		} = this.props;

		const scale = this.scrollY.interpolate({
			inputRange: [-10, 0],
			outputRange: [0.6, 1],
			extrapolate: 'clamp',
		});
		const translateY = this.scrollY.interpolate({
			inputRange: [-10, 0],
			outputRange: [0, 0],
			extrapolate: 'clamp',
		});

		const animStyle = {
			transform: [{ scale }, { translateY }],
		};

		return (
			<View style={[styles.screenContainer, { backgroundColor: 'transparent' }]}>
				<StatusBar hidden />
				<AnimatedScrollView
					style={[{ flex: 1, width: '100%' }]}
					contentContainerStyle={{
						flexGrow: 1,
						// paddingBottom: 20,
					}}
					onMomentumScrollBegin={this.onMomentumScrollBeginHandler}
					onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.scrollY } } }], {
						useNativeDriver: true,
					})}
				>
					<Animated.View style={[taskStyles.shadow, animStyle]}>
						<View
							style={{
								width: '100%',
								height: height * 0.75,
								zIndex: 999,
							}}
						>
							<TouchableOpacity onPress={this.onPressHandler} style={taskStyles.iconContainer}>
								<Ionicons name="md-close" color="#fff" size={20} />
							</TouchableOpacity>
							<Transition shared={`${challenge._id}`}>
								<FastImage
									source={{ uri: `${challenge.image.url}` }}
									resizeMode={FastImage.resizeMode.stretch}
									style={taskStyles.challengeImageStyle}
								/>
							</Transition>
							<Transition shared={`title${challenge._id}`}>
								<Text
									style={[
										taskStyles.challengeTitleStyle,
										{
											color: challenge.titleColor,
										},
									]}
								>
									{challenge.title}
								</Text>
							</Transition>
							{this.renderFooter()}
						</View>
						<Transition appear={contentAppearTransition}>
							<View
								style={{
									padding: 20,
									backgroundColor: iOSColors.white,
									zIndex: -999,
								}}
							>
								<Markdown style={{ text: { fontSize: 15 } }}>{challenge.body}</Markdown>
								<Button
									title="Go back"
									onPress={this.onPressHandler}
									containerStyle={{ marginTop: 20 }}
								/>
							</View>
						</Transition>
					</Animated.View>
				</AnimatedScrollView>
			</View>
		);
	}
}

export default Task;
