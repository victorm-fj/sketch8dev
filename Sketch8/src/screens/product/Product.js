/**
 * @format
 * @flow
 */

import React from 'react';
import {
	ScrollView,
	View,
	StatusBar,
	Text,
	Dimensions,
	Animated,
	TouchableOpacity,
} from 'react-native';
import { Transition } from 'react-navigation-fluid-transitions';
import { Button } from 'react-native-elements';
import type { NavigationScreenProp } from 'react-navigation';
import { iOSUIKit, iOSColors } from 'react-native-typography';
import Markdown from 'react-native-markdown-renderer';
import FastImage from 'react-native-fast-image';
import Swiper from 'react-native-swiper';
import Ionicons from 'react-native-vector-icons/Ionicons';

import styles from '../../styles';
import productStyles from './styles';

const { width, height } = Dimensions.get('window');

const SERVER_URL = 'http://zilln.com';

const contentAppearTransition = transitionInfo => {
	const { progress, start, end, boundingbox, direction } = transitionInfo;

	const distanceValue = -(boundingbox.height + boundingbox.y + 25);
	let startValue = 0;
	let endValue = distanceValue;

	if (direction === 2) {
		startValue = distanceValue;
		endValue = 0;
	}

	const translateY = progress.interpolate({
		inputRange: [0, start, end, 1],
		outputRange: [startValue, startValue, endValue, endValue],
	});

	const opacity = progress.interpolate({
		inputRange: [0, start, end, 1],
		outputRange: [0, 0, 0.05, 1],
	});

	return {
		transform: [
			{
				translateY,
			},
		],
		opacity,
	};
};

const contentDisappearTransition = transitionInfo => {
	const { progress, start, end } = transitionInfo;
	const opacity = progress.interpolate({
		inputRange: [0, start, end, 1],
		outputRange: [1, 0.75, 0.5, 0],
	});

	return {
		opacity,
	};
};

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const nextButton = (
	<View
		style={{
			width: width * 0.2,
			height: height * 0.5,
			justifyContent: 'center',
			alignItems: 'flex-end',
		}}
	>
		<Text
			style={{
				fontSize: 50,
				color: '#007aff',
			}}
		>
			›
		</Text>
	</View>
);

const prevButton = (
	<View
		style={{
			width: width * 0.2,
			height: height * 0.5,
			justifyContent: 'center',
			alignItems: 'flex-start',
		}}
	>
		<Text
			style={{
				fontSize: 50,
				color: '#007aff',
			}}
		>
			‹
		</Text>
	</View>
);

type Props = { navigation: NavigationScreenProp<*> };
type State = { enableScrollViewScroll: boolean };
class Product extends React.Component<Props, State> {
	// state = { enableScrollViewScroll: true };

	scrollY = new Animated.Value(0);

	onPressHandler = () => {
		const { navigation } = this.props;
		navigation.goBack();
	};

	onPressGetHandler = () => {};

	onMomentumScrollBeginHandler = (event: Object) => {
		const {
			nativeEvent: {
				contentOffset: { y },
			},
		} = event;
		if (y < -40) {
			this.onPressHandler();
		}
	};

	onPressImageHandler = (index: number) => {
		const { navigation } = this.props;
		const {
			state: {
				params: {
					product: { images },
				},
			},
		} = navigation;
		navigation.navigate('ProductGallery', { images, index });
	};

	renderFooter() {
		const {
			navigation: {
				state: {
					params: {
						product: { _id, name, price },
					},
				},
			},
		} = this.props;

		return (
			<View style={productStyles.footerContainer}>
				<Transition shared={`footer${_id}`}>
					<View style={productStyles.footerBackground} />
				</Transition>

				<View style={productStyles.rowContainer}>
					<View style={{ flex: 1 }}>
						<Transition shared={`footerTitle${_id}`}>
							<Text style={[iOSUIKit.subheadEmphasized, productStyles.footerTitle]}>{name}</Text>
						</Transition>

						<Transition shared={`footerSubtitle${_id}`}>
							<Text style={[iOSUIKit.footnote, productStyles.footerSubtitle]}>$ {price}</Text>
						</Transition>
					</View>

					<Transition shared={`footerAction${_id}`}>
						<Button
							title="GET"
							titleStyle={productStyles.footerButtonTitle}
							buttonStyle={productStyles.footerButton}
							onPress={this.onPressGetHandler}
							containerStyle={{ marginTop: 3 }}
						/>
					</Transition>
				</View>
			</View>
		);
	}

	render() {
		// const { enableScrollViewScroll } = this.state;
		const {
			navigation: {
				state: {
					params: {
						product: { _id, description, images },
					},
				},
			},
		} = this.props;

		const scale = this.scrollY.interpolate({
			inputRange: [-10, 0],
			outputRange: [0.6, 1],
			extrapolate: 'clamp',
		});
		const translateY = this.scrollY.interpolate({
			inputRange: [-10, 0],
			outputRange: [0, 0],
			extrapolate: 'clamp',
		});

		const animStyle = {
			transform: [{ scale }, { translateY }],
		};

		return (
			<View
				style={[styles.screenContainer, { backgroundColor: 'transparent' }]}
				// onStartShouldSetResponderCapture={() => {
				// 	this.setState({ enableScrollViewScroll: true });
				// }}
			>
				<StatusBar hidden />
				<AnimatedScrollView
					style={{ flex: 1, width: '100%' }}
					contentContainerStyle={{ flexGrow: 1 }}
					onMomentumScrollBegin={this.onMomentumScrollBeginHandler}
					onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.scrollY } } }], {
						useNativeDriver: true,
					})}
					// scrollEnabled={enableScrollViewScroll}
				>
					<Animated.View style={[productStyles.shadow, animStyle]}>
						<View style={{ width: '100%', height: height * 0.5, zIndex: 999 }}>
							<TouchableOpacity onPress={this.onPressHandler} style={productStyles.iconContainer}>
								<Ionicons name="md-close" color="#fff" size={20} />
							</TouchableOpacity>
							<Transition shared={`image${_id}`}>
								<View
									style={{ flex: 1 }}
									// onStartShouldSetResponderCapture={() => {
									// 	this.setState({ enableScrollViewScroll: false });
									// 	if (enableScrollViewScroll === false) {
									// 		this.setState({ enableScrollViewScroll: true });
									// 	}
									// }}
								>
									<Swiper
										style={{ flex: 1, width: '100%' }}
										showsButtons
										removeClippedSubviews={false}
										loop={false}
										nextButton={nextButton}
										prevButton={prevButton}
									>
										{images.map((image, index) => (
											<TouchableOpacity
												key={index}
												style={{ flex: 1 }}
												onPress={() => this.onPressImageHandler(index)}
											>
												<FastImage
													source={{ uri: `${image.url}` }}
													resizeMode={FastImage.resizeMode.stretch}
													style={productStyles.challengeImageStyle}
												/>
											</TouchableOpacity>
										))}
									</Swiper>
								</View>
							</Transition>
							{this.renderFooter()}
						</View>
						<Transition appear={contentAppearTransition} disappear={contentDisappearTransition}>
							<View
								style={{
									padding: 20,
									backgroundColor: iOSColors.white,
									zIndex: -999,
								}}
							>
								<Markdown style={{ text: { fontSize: 15 } }}>{description}</Markdown>
								<Button
									title="Go back"
									onPress={this.onPressHandler}
									containerStyle={{ marginTop: 20 }}
								/>
							</View>
						</Transition>
					</Animated.View>
				</AnimatedScrollView>
			</View>
		);
	}
}

export default Product;
