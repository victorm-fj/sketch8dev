import { StyleSheet, Platform } from 'react-native';
import { iOSColors } from 'react-native-typography';

export default StyleSheet.create({
	footerContainer: {
		width: '100%',
		paddingHorizontal: 20,
		paddingVertical: 15,
		height: 70,
		position: 'absolute',
		left: 0,
		bottom: 0,
	},
	rowContainer: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	footerButton: {
		backgroundColor: iOSColors.blue,
		borderRadius: 30,
		paddingHorizontal: 10,
	},
	footerButtonTitle: {
		color: iOSColors.white,
		fontWeight: 'bold',
		fontSize: 15,
	},
	iconContainer: {
		position: 'absolute',
		top: 15,
		right: 15,
		backgroundColor: 'rgba(0,0,0,0.5)',
		width: 28,
		height: 28,
		borderRadius: 14,
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: 999,
	},
	challengeTitleStyle: {
		fontSize: 40,
		fontWeight: 'bold',
		color: 'white',
		position: 'absolute',
		left: 20,
		bottom: 80,
		width: 150,
		lineHeight: 40,
	},
	challengeImageStyle: {
		flex: 1,
		width: '100%',
	},
	shadow: {
		...Platform.select({
			ios: {
				shadowColor: 'black',
				shadowOpacity: 0.3,
				shadowRadius: 15,
				shadowOffset: {
					width: 0,
					height: 15,
				},
			},
			android: {
				elevation: 6,
			},
		}),
	},
	footerBackground: {
		backgroundColor: 'rgba(255,255,255,0.5)',
		height: 70,
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
	},
	footerTitle: {
		position: 'absolute',
		top: -18,
		left: 0,
	},
	footerSubtitle: {
		position: 'absolute',
		top: 5,
		left: 0,
	},
});
