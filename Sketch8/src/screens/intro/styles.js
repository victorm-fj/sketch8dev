import { StyleSheet } from 'react-native';
import { iOSColors } from 'react-native-typography';

const colors = {
	white: '#f9fafc',
	black: '#3b4859',
	gray: '#8392a7',
	purple: iOSColors.purple,
};

export default StyleSheet.create({
	screenContainer: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: colors.white,
	},
	videoContainer: {
		backgroundColor: 'rgba(0,0,0,0.7)',
		position: 'absolute',
		right: 10,
		bottom: 10,
		width: 26,
		height: 26,
		borderRadius: 13,
		justifyContent: 'center',
		alignItems: 'center',
	},
	actionsContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	actionsAuthContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonContainer: {
		width: '50%',
	},
	baseButton: {
		height: 40,
		width: '90%',
		borderRadius: 5,
		borderWidth: 1,
		alignSelf: 'center',
	},
	signUpButton: {
		backgroundColor: colors.purple,
		borderColor: colors.purple,
		marginRight: -5,
	},
	signUpButtonTitle: {
		color: colors.white,
	},
	signInButton: {
		borderRadius: 5,
		borderColor: colors.black,
		marginLeft: -5,
	},
	signInButtonTitle: {
		color: colors.black,
	},
	footnoteContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 5,
	},
	footnoteText: {
		fontSize: 14,
		color: colors.gray,
		marginRight: 5,
	},
});
