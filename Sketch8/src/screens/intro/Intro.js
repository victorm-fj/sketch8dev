/**
 * @format
 * @flow
 */
import React from 'react';
import { View, StatusBar, TouchableWithoutFeedback, Image, Dimensions, Text } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import Video from 'react-native-video';
import Orientation from 'react-native-orientation';
import type { NavigationScreenProp } from 'react-navigation';

import { Slider, Slide } from '../../components/slider';
import SketchCanvas from '../canvas/SketchCanvas';
import styles from './styles';
import video from './adobe-comp-cc.mp4';
import image from './slider3-image.png';
import facebook from './facebook.png';

const { height } = Dimensions.get('window');
const slides = [
	{
		id: 0,
		title: 'Explore Ideas Quickly',
		body:
			'Iterate quickly through multiple design ideas. Combine the speed of drawing with the fidelity of real design elements.',
	},
	{
		id: 1,
		title: 'Sketch to Create',
		body: 'Start sketching your layout and Comp will convert your sketch into crisp graphics.',
	},
	{
		id: 2,
		title: 'Connect to Creative Cloud',
		body:
			'Your projects are always backed up. Access your creative files and send your projects to Photoshop, Ilustrator, and InDesign.',
	},
];

type Props = { navigation: NavigationScreenProp<*> };
type State = {
	sliderIndex: number,
	videoMuted: boolean,
	scrollEnabled: boolean,
};
class Intro extends React.Component<Props, State> {
	state = { sliderIndex: 0, videoMuted: true, scrollEnabled: true };

	componentDidMount() {
		Orientation.lockToPortrait();
	}

	// componentWillUnmount() {
	// 	Orientation.unlockAllOrientations();
	// }

	onSignUp = () => {
		const { navigation } = this.props;
		navigation.navigate('SignUp');
	};

	onSignIn = () => {
		const { navigation } = this.props;
		navigation.navigate('SignIn');
	};

	toggleVideoSound = () => {
		this.setState(prevState => ({ videoMuted: !prevState.videoMuted }));
	};

	updateSliderIndex = (sliderIndex: number) => {
		this.setState({ sliderIndex });
	};

	onStartDrawing = () => {
		this.setState({ scrollEnabled: false });
	};

	onEndDrawing = () => {
		this.setState({ scrollEnabled: true });
	};

	render() {
		const { sliderIndex, videoMuted, scrollEnabled } = this.state;
		return (
			<View style={styles.screenContainer}>
				<StatusBar hidden />
				<Slider updateIndex={this.updateSliderIndex} scrollEnabled={scrollEnabled}>
					<Slide data={slides[0]}>
						<TouchableWithoutFeedback onPress={this.toggleVideoSound}>
							<View>
								<Video
									source={video}
									style={{ width: '100%', height: '100%' }}
									repeat
									resizeMode="cover"
									paused={sliderIndex !== 0}
									muted={videoMuted}
								/>
								<Icon
									type="ionicon"
									name={videoMuted ? 'ios-volume-off' : 'ios-volume-high'}
									color="#fff"
									size={20}
									containerStyle={styles.videoContainer}
								/>
							</View>
						</TouchableWithoutFeedback>
					</Slide>
					<Slide data={slides[1]}>
						<SketchCanvas
							onStartDrawing={this.onStartDrawing}
							onEndDrawing={this.onEndDrawing}
							height={height * 0.6}
						/>
					</Slide>
					<Slide data={slides[2]}>
						<Image source={image} resizeMode="cover" style={{ width: '100%', height: '100%' }} />
					</Slide>
				</Slider>
				<View style={styles.actionsContainer}>
					<View style={styles.actionsAuthContainer}>
						<Button
							title="Sign Up"
							onPress={this.onSignUp}
							containerStyle={styles.buttonContainer}
							buttonStyle={[styles.baseButton, styles.signUpButton]}
							titleStyle={styles.signUpButtonTitle}
						/>
						<Button
							title="Sign In"
							clear
							onPress={this.onSignIn}
							containerStyle={styles.buttonContainer}
							buttonStyle={[styles.baseButton, styles.signInButton]}
							titleStyle={styles.signInButtonTitle}
						/>
					</View>
					<View style={styles.footnoteContainer}>
						<Text style={styles.footnoteText}>Sign in with</Text>
						<Image source={facebook} style={{ height: 25, width: 75 }} resizeMode="contain" />
					</View>
				</View>
			</View>
		);
	}
}

export default Intro;
