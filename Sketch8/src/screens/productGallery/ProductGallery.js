/**
 * @format
 * @flow
 */

import React from 'react';
import type { NavigationScreenProp } from 'react-navigation';
import Gallery from 'react-native-photo-gallery';
import { View, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import productStyles from '../product/styles';

const SERVER_URL = 'http://zilln.com';

type Props = { navigation: NavigationScreenProp<*> };
class ProductGallery extends React.Component<Props> {
	onPressHandler = () => {
		const { navigation } = this.props;
		navigation.goBack();
	};

	render() {
		const {
			navigation: {
				state: {
					params: { images, index },
				},
			},
		} = this.props;

		return (
			<View style={{ flex: 1 }}>
				<TouchableOpacity
					onPress={this.onPressHandler}
					style={[productStyles.iconContainer, { backgroundColor: 'rgba(255,255,255,0.5)' }]}
				>
					<Ionicons name="md-close" color="#000" size={20} />
				</TouchableOpacity>
				<Gallery
					initialIndex={index}
					data={images.map(({ _id, url }) => ({
						id: _id,
						image: { uri: `${url}` },
						thumb: { uri: `${url}` },
					}))}
				/>
			</View>
		);
	}
}

export default ProductGallery;
