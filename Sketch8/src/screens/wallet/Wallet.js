/**
 * @format
 * @flow
 */

import React from 'react';
import { View, Text } from 'react-native';
import { iOSUIKit, iOSColors } from 'react-native-typography';
import { graphql } from 'react-apollo';

import { MeQuery } from '../../graphQL';
import Loader from '../../components/Loader';
import styles from '../../styles';

type Props = { me: Object, loading: Boolean };
type State = {};
class Wallet extends React.Component<Props, State> {
	state = {};

	render() {
		const { loading, me } = this.props;
		if (loading) {
			return <Loader />;
		}

		return (
			<View style={[styles.screenContainer, { padding: 20 }]}>
				<Text style={[iOSUIKit.title3Emphasized, { marginTop: 40, marginBottom: 20 }]}>
					Your wallet
				</Text>
				<Text style={[iOSUIKit.largeTitleEmphasized, { marginBottom: 20 }]}>
					$ {me ? me.coins : '25'}
				</Text>
				<Text style={[iOSUIKit.bodyEmphasized, { color: iOSColors.gray, alignSelf: 'flex-start' }]}>
					Transaction History
				</Text>
			</View>
		);
	}
}

export default graphql(MeQuery, {
	options: {
		fetchPolicy: 'network-only',
	},
	props: props => ({ me: props.data.me, loading: props.data.loading }),
})(Wallet);
