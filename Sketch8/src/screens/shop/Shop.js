/**
 * @format
 * @flow
 */

import React from 'react';
import { View, StatusBar, FlatList } from 'react-native';
import type { NavigationScreenProp } from 'react-navigation';
import { request } from 'graphql-request';

import Loader from '../../components/Loader';
import ProductItem from '../../components/productItem/ProductItem';
import styles from '../../styles';
import todayStyles from '../today/todayStyles';

const SERVER_URL = 'http://zilln.com';

type Props = { navigation: NavigationScreenProp<*> };
type State = { products: Array<Object>, loading: boolean };
class Product extends React.Component<Props, State> {
	state = { products: [], loading: true };

	async componentDidMount() {
		try {
			const query = `{
				products(
					where: {
						release: true
					}
				) {
					_id
          name
          price
          description
          images {
            _id
            url
          }
				}
			}`;
			const result = await request(`${SERVER_URL}/graphql`, query);
			console.log('fetchProducts result:', result);
			this.setState({ products: result.products, loading: false });
		} catch (error) {
			console.log('fetchProducts error:', error);
			this.setState({ loading: false });
		}
	}

	onPressHandler = product => {
		const { navigation } = this.props;
		navigation.navigate('Product', { product });
	};

	renderItem = ({ item }) => <ProductItem item={item} onPress={() => this.onPressHandler(item)} />;

	render() {
		const { loading, products } = this.state;
		if (loading) {
			return <Loader />;
		}

		return (
			<View style={styles.screenContainer}>
				<StatusBar hidden={false} />
				<FlatList
					style={todayStyles.scrollViewContainer}
					contentContainerStyle={todayStyles.scrollViewContent}
					data={products}
					extraData={this.state}
					renderItem={this.renderItem}
					keyExtractor={item => item._id}
				/>
			</View>
		);
	}
}

export default Product;
