/**
 * @format
 * @flow
 */

const hText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
const pText =
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ut luctus nibh. Morbi sit amet faucibus purus. Sed a sagittis metus. Nulla eget dui vitae velit mollis elementum. Integer sodales commodo augue nec venenatis. Nunc sollicitudin efficitur risus, sed lobortis lorem bibendum eu. Vivamus vel nulla congue, mattis urna sit amet, consectetur diam. Suspendisse condimentum faucibus diam in dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis erat quam. Nunc pretium eros eu tempor dictum. Nunc tempus quam ut egestas sollicitudin.';

const findCenter = (points) => {
	let x = 0;
	let y = 0;
	const len = points.length;
	for (let i = 0; i < len; i += 1) {
		x += points[i].x;
		y += points[i].y;
	}
	return {
		x: Math.round((x / len) * 100) / 100,
		y: Math.round((y / len) * 100) / 100,
	};
};

const findAngle = (c, p) => {
	const dx = p.x - c.x;
	const dy = p.y - c.y;
	const angleDeg = (Math.atan2(dy, dx) * 180) / Math.PI;
	const angle = Math.round(angleDeg * 100) / 100;
	return { ...p, angle };
};

export const twoPointsDistance = (p1: Object, p2: Object): number => {
	const dx = p2.x - p1.x;
	const dy = p2.y - p1.y;
	return Math.sqrt(dx * dx + dy * dy);
};

const wrapText = async (ctx, text, x, y, lineHeight, maxWidth, maxHeight) => {
	const words = text.split(' ');
	let line = '';
	let yPos = y + lineHeight;
	let currentHeight = 0;

	for (let i = 0; i < words.length; i += 1) {
		if (currentHeight < maxHeight - lineHeight) {
			const testLine = `${line + words[i]} `;
			const metrics = await ctx.measureText(testLine);
			const testWidth = metrics.width;
			if (testWidth > maxWidth && i > 0) {
				ctx.fillText(line, x, yPos);
				line = `${words[i]} `;
				yPos += lineHeight;
				currentHeight += lineHeight;
			} else {
				line = testLine;
			}
		}
	}
};

export const drawPerimeter = (ctx: any, path: Object) => {
	// Find center
	const centerP = findCenter(path.data);
	// Find angles
	const anglesP = path.data.map((p) => findAngle(centerP, p));
	// Sort based on angle
	anglesP.sort((a, b) => (a.angle >= b.angle ? 1 : -1));
	// Draw
	ctx.beginPath();
	ctx.moveTo(anglesP[0].x, anglesP[0].y);
	for (let i = 0; i < anglesP.length; i += 1) {
		ctx.lineTo(anglesP[i].x, anglesP[i].y);
	}
	ctx.strokeStyle = '#000';
	// this.ctx.closePath();
	ctx.stroke();
};

export const drawRectangle = (ctx: any, path: Object) => {
	let origin = path.data[0];
	const widthCoords = [];
	const heightCoords = [];

	const widthCoordsObts = [];
	const heightCoordsObts = [];

	// Find center
	const centerP = findCenter(path.data);

	path.data.forEach((point) => {
		const dx = origin.x - point.x;
		const dy = origin.y - point.y;
		let theta = Math.atan2(-dy, -dx);
		theta *= 180 / Math.PI;
		if (theta < 0) theta += 360;
		if (theta > 80 && theta < 100) {
			heightCoords.push(point.y);
		}
		if (theta < 25) {
			widthCoords.push(point.x);
		}
		if (theta > 250 && theta < 290) {
			heightCoordsObts.push(point.y);
		}
		if (theta > 160 && theta < 200) {
			widthCoordsObts.push(point.x);
		}
	});

	const maxWidth = Math.max(...widthCoords);
	const maxHeight = Math.max(...heightCoords);
	let width = maxWidth - origin.x;
	let height = maxHeight - origin.y;

	// If reactangle is drawn beginning from bottom right point
	if (!Number.isFinite(width) || !Number.isFinite(height)) {
		const minHeight = Math.min(...heightCoordsObts);
		const minWidth = Math.min(...widthCoordsObts);
		width = origin.x - minWidth;
		height = origin.y - minHeight;
		origin = { x: minWidth, y: minHeight };
	}

	ctx.fillStyle = 'pink';
	ctx.fillRect(origin.x, origin.y, width, height);

	const drawnShape = {
		id: path.id,
		shape: 'rectangle',
		origin: { x: origin.x, y: origin.y },
		center: { x: centerP.x, y: centerP.y },
		width,
		height,
	};

	return drawnShape;
};

export const eraseRectangle = (ctx: any, shape: Object) => {
	const { origin } = shape;
	// this.ctx.clearRect(origin.x, origin.y, shape.width, shape.height);
	ctx.fillStyle = '#eeeeee';
	ctx.fillRect(origin.x, origin.y, shape.width, shape.height);
};

export const drawCircle = (ctx: any, path: Object): Object => {
	const origin = path.data[0];
	// Find center
	const centerP = findCenter(path.data);
	const dx = centerP.x - origin.x;
	const dy = centerP.y - origin.y;
	// Radius
	const r = Math.sqrt(dx * dx + dy * dy);
	ctx.fillStyle = 'pink';
	// Draw circle
	ctx.beginPath();
	ctx.arc(centerP.x, centerP.y, r, 0, 2 * Math.PI);
	// this.ctx.closePath();
	ctx.fill();

	const drawnShape = {
		id: path.id,
		shape: 'circle',
		center: { x: centerP.x, y: centerP.y },
		radius: r,
	};
	return drawnShape;
};

export const eraseCircle = (ctx: any, shape: Object) => {
	const { center, radius } = shape;
	// this.ctx.clearRect(
	// 	center.x - radius,
	// 	center.y - radius,
	// 	radius * 2,
	// 	radius * 2
	// );
	ctx.fillStyle = '#eeeeee';
	ctx.fillRect(center.x - radius, center.y - radius, radius * 2, radius * 2);
};

export const drawImage = (ctx: any, path: Object): Object => {
	let origin = path.data[0];
	const widthCoords = [];
	const heightCoords = [];

	const widthCoordsObts = [];
	const heightCoordsObts = [];

	// Find center
	const centerP = findCenter(path.data);

	path.data.forEach((point) => {
		const dx = origin.x - point.x;
		const dy = origin.y - point.y;
		let theta = Math.atan2(-dy, -dx);
		theta *= 180 / Math.PI;
		if (theta < 0) theta += 360;
		if (theta > 80 && theta < 100) {
			heightCoords.push(point.y);
		}
		if (theta < 25) {
			widthCoords.push(point.x);
		}
		if (theta > 250 && theta < 290) {
			heightCoordsObts.push(point.y);
		}
		if (theta > 160 && theta < 200) {
			widthCoordsObts.push(point.x);
		}
	});

	const maxWidth = Math.max(...widthCoords);
	const maxHeight = Math.max(...heightCoords);
	let width = maxWidth - origin.x;
	let height = maxHeight - origin.y;

	// If reactangle is drawn beginning from bottom right point
	if (!Number.isFinite(width) || !Number.isFinite(height)) {
		const minHeight = Math.min(...heightCoordsObts);
		const minWidth = Math.min(...widthCoordsObts);
		width = origin.x - minWidth;
		height = origin.y - minHeight;
		origin = { x: minWidth, y: minHeight };
	}

	ctx.fillStyle = 'pink';
	ctx.strokeStyle = 'white';
	ctx.fillRect(origin.x, origin.y, width, height);
	ctx.beginPath();
	ctx.moveTo(origin.x, origin.y);
	ctx.lineTo(origin.x + width, origin.y + height);
	ctx.stroke();
	ctx.moveTo(origin.x, origin.y + height);
	ctx.lineTo(origin.x + width, origin.y);
	ctx.stroke();

	const drawnShape = {
		id: path.id,
		shape: 'rectangle',
		origin: { x: origin.x, y: origin.y },
		center: { x: centerP.x, y: centerP.y },
		width,
		height,
	};
	return drawnShape;
};

export const drawParagraph = (ctx: any, path: Object): any => {
	const origin = path.data[0];
	const widthCoords = [];
	const heightCoords = [];

	// Find center
	const centerP = findCenter(path.data);

	path.data.forEach((point) => {
		const dx = origin.x - point.x;
		const dy = origin.y - point.y;
		let theta = Math.atan2(-dy, -dx);
		theta *= 180 / Math.PI;
		if (theta < 0) theta += 360;
		if (theta > 80 && theta < 100) {
			heightCoords.push(point.y);
		}
		if (theta < 25) {
			widthCoords.push(point.x);
		}
	});

	const maxWidth = Math.max(...widthCoords);
	const maxHeight = Math.max(...heightCoords);
	const width = maxWidth - origin.x;
	const height = maxHeight - origin.y;

	if (Number.isNaN(width) || Number.isNaN(height)) {
		return null;
	}

	ctx.font = '10pt Helvetica';
	ctx.fillStyle = '#333';
	const lineHeight = 12;
	wrapText(ctx, pText, origin.x, origin.y, lineHeight, width, height);

	const drawnShape = {
		id: path.id,
		shape: 'rectangle',
		origin: { x: origin.x, y: origin.y },
		center: { x: centerP.x, y: centerP.y },
		width,
		height,
	};
	return drawnShape;
};

export const drawHeadline = (ctx: any, path: Object): any => {
	const origin = path.data[0];
	// Find center
	const centerP = findCenter(path.data);
	const lastPoint = path.data[path.data.length - 1];
	const width = twoPointsDistance(origin, lastPoint);
	if (Number.isNaN(width)) {
		return null;
	}
	ctx.font = '18pt Helvetica';
	ctx.fillStyle = '#333';
	const lineHeight = 20;
	const height = lineHeight + 6;

	const updatedOrigin = { x: origin.x, y: origin.y - lineHeight };

	wrapText(
		ctx,
		hText,
		updatedOrigin.x,
		updatedOrigin.y,
		lineHeight,
		width,
		height
	);

	const drawnShape = {
		id: path.id,
		shape: 'rectangle',
		origin: updatedOrigin,
		center: { x: centerP.x, y: centerP.y - lineHeight },
		width,
		height,
	};
	return drawnShape;
};
