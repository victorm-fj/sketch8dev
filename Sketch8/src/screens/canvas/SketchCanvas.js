/**
 * @format
 * @flow
 */
import React from 'react';
import { View, Dimensions, Image } from 'react-native';
import { SketchCanvas } from '@terrylinla/react-native-sketch-canvas';
import Canvas from 'react-native-canvas';
import ShapeDetector from 'shape-detector';
import { Button } from 'react-native-elements';

import {
	drawRectangle,
	drawCircle,
	eraseRectangle,
	eraseCircle,
	drawImage,
	drawParagraph,
	drawHeadline,
	twoPointsDistance,
} from './drawing';
import erasers from './erasers';
import circles from './circles';
import images from './images';
import headings from './headings';
import paragraphs from './paragraphs';
import rectangles from './rectangles';
import shapes from './shapes.png';
import styles from './styles';
import verticalLines from './verticalLines';

const { width, height } = Dimensions.get('window');

type xYPath = { id: number, data: Array<Object> };
type Shape = {
	id: number,
	shape: string,
	origin?: { x: number, y: number },
	center?: { x: number, y: number },
	width?: number,
	height?: number,
	radius?: number,
};
type Props = {
	onStartDrawing?: Function,
	onEndDrawing?: Function,
	width?: number,
	height?: number,
};
type State = {
	xYPaths: Array<xYPath>,
	drawnShapes: Array<Shape>,
};
class SketchCanvasComponent extends React.Component<Props, State> {
	static defaultProps = {
		onStartDrawing: () => {},
		onEndDrawing: () => {},
		width,
		height,
	};

	constructor(props) {
		super(props);
		this.detector = new ShapeDetector(ShapeDetector.defaultShapes);
		rectangles.forEach(rectangle => this.detector.learn('rectangle', rectangle));
		circles.forEach(circle => this.detector.learn('circle', circle));
		erasers.forEach(eraser => this.detector.learn('eraser', eraser));
		images.forEach(image => this.detector.learn('image', image));
		headings.forEach(heading => this.detector.learn('heading', heading));
		paragraphs.forEach(paragraph => this.detector.learn('paragraph', paragraph));
		verticalLines.forEach(verticalLine => this.detector.learn('verticalLine', verticalLine));

		this.state = { xYPaths: [], drawnShapes: [] };
	}

	componentDidMount() {
		if (this.canvas) {
			this.canvas.width = this.props.width;
			this.canvas.height = this.props.height;
			this.ctx = this.canvas.getContext('2d');
			this.ctx.fillStyle = '#eeeeee';
			this.ctx.fillRect(0, 0, width, height);
		}
	}

	clear = () => {
		if (this.sketchCanvas) {
			this.sketchCanvas.clear();
		}
		if (this.canvas) {
			this.ctx.fillStyle = '#eeeeee';
			this.ctx.fillRect(0, 0, width, height);
			// this.ctx.clearRect(0, 0, width, height);
		}
		this.setState({ xYPaths: [], drawnShapes: [] });
	};

	drawCanvas = path => {
		const shape = this.detector.spot([...path.data]);
		console.log('shape', shape);

		if (!shape.pattern) {
			this.removeFromSketch(path.id);
		} else if (this.ctx) {
			switch (shape.pattern) {
				case 'triangle':
				case 'rectangle':
				case 'square':
					this.drawRectangle(path);
					break;
				case 'circle':
					this.drawCircle(path);
					break;
				case 'paragraph':
					this.drawParagraph(path);
					break;
				case 'heading':
					this.drawHeadline(path);
					break;
				case 'image':
					this.drawImage(path);
					break;
				case 'eraser':
					this.handleEraser(path);
					break;
				default:
					this.removeFromSketch(path.id);
					// this.drawPerimeter(path, ctx);
					break;
			}
		}
	};

	drawHeadline = (path: Object) => {
		const drawnShape = drawHeadline(this.ctx, path);
		if (drawnShape) {
			this.removeFromSketchAndUpdateDrawnShapes(path.id, drawnShape);
		} else {
			this.removeFromSketch(path.it);
		}
	};

	drawParagraph = (path: Object) => {
		const drawnShape = drawParagraph(this.ctx, path);
		if (drawnShape) {
			this.removeFromSketchAndUpdateDrawnShapes(path.id, drawnShape);
		} else {
			this.removeFromSketch(path.it);
		}
	};

	drawImage = (path: Object) => {
		const drawnShape = drawImage(this.ctx, path);
		this.removeFromSketchAndUpdateDrawnShapes(path.id, drawnShape);
	};

	handleEraser = (path: Object) => {
		const { drawnShapes } = this.state;
		this.removeFromSketch(path.id);

		const eraserFirstPoint = path.data[0];
		drawnShapes.forEach(shape => {
			const distanceBetween = twoPointsDistance(eraserFirstPoint, shape.center);
			if (shape.shape === 'circle' && distanceBetween < shape.radius) {
				this.checkAllEraserPoints(path, shape, shape.radius);
			} else if (shape.shape === 'rectangle') {
				let distance = shape.height / 2;
				if (shape.width > shape.height) {
					distance = shape.width / 2;
				}
				if (distanceBetween < distance) {
					this.checkAllEraserPoints(path, shape, distance);
				}
			}
		});
	};

	checkAllEraserPoints = (path: Object, shape: Object, distance: number) => {
		const { center } = shape;
		let isInside = true;
		path.data.forEach(point => {
			const distanceBetween = twoPointsDistance(point, center);
			if (distanceBetween > distance) {
				isInside = false;
			}
		});
		if (isInside) {
			this.removeFromCanvas(shape);
		}
	};

	removeFromSketch = (pathId: string) => {
		if (this.sketchCanvas) {
			this.sketchCanvas.deletePath(pathId);
		}
		this.setState(prevState => ({
			xYPaths: prevState.xYPaths.filter(path => path.id !== pathId),
		}));
	};

	removeFromCanvas = (shape: Object) => {
		this.setState(prevState => ({
			drawnShapes: prevState.drawnShapes.filter(drawnShape => drawnShape.id !== shape.id),
		}));
		if (shape.shape === 'circle') {
			this.removeCircleFromCanvas(shape);
		} else if (shape.shape === 'rectangle') {
			this.removeRectangleFromCanvas(shape);
		}
	};

	removeCircleFromCanvas = (shape: Object) => {
		eraseCircle(this.ctx, shape);
	};

	removeRectangleFromCanvas = (shape: Object) => {
		eraseRectangle(this.ctx, shape);
	};

	drawCircle = (path: Object) => {
		const drawnShape = drawCircle(this.ctx, path);
		this.removeFromSketchAndUpdateDrawnShapes(path.id, drawnShape);
	};

	drawRectangle = (path: Object) => {
		const drawnShape = drawRectangle(this.ctx, path);
		this.removeFromSketchAndUpdateDrawnShapes(path.id, drawnShape);
	};

	removeFromSketchAndUpdateDrawnShapes = (pathId: string, drawnShape: Object) => {
		if (this.sketchCanvas) {
			this.sketchCanvas.deletePath(pathId);
		}
		// Also update drawnShapes component state
		this.setState(prevState => ({
			xYPaths: prevState.xYPaths.filter(path => path.id !== pathId),
			drawnShapes: [...prevState.drawnShapes, drawnShape],
		}));
	};

	getPaths = () => {
		if (this.sketchCanvas) {
			const paths = this.sketchCanvas.getPaths();

			const sketchXYPaths = paths.map(({ path }) => {
				const data = path.data.map(points => {
					const [x, y] = points.split(',');
					return { x: Number(x), y: Number(y) /* angle: 0 */ };
				});
				return { id: path.id, data };
			});

			const newPath = sketchXYPaths[0];
			console.log('newPath', newPath);

			this.setState(prevState => ({
				xYPaths: [...prevState.xYPaths, newPath],
			}));
			this.drawCanvas(newPath);
		}
	};

	onStrokeEndHandler = () => {
		setTimeout(this.getPaths, 300);
		const { onEndDrawing } = this.props;
		if (onEndDrawing) {
			onEndDrawing();
		}
	};

	onStrokeStartHandler = () => {
		const { onStartDrawing } = this.props;
		if (onStartDrawing) {
			onStartDrawing();
		}
	};

	render() {
		const { xYPaths, drawnShapes } = this.state;
		const isClearDisabled = xYPaths.length === 0 && drawnShapes.length === 0;
		return (
			<View style={styles.container}>
				<View
					style={[
						{
							width,
							height,
						},
						styles.canvasContainer,
					]}
				>
					<Canvas ref={canvas => (this.canvas = canvas)} />
				</View>

				<Image source={shapes} style={styles.shapesContainer} resizeMode="contain" />

				<View style={styles.sketchCanvasContainer}>
					<SketchCanvas
						ref={sketchCanvas => (this.sketchCanvas = sketchCanvas)}
						style={styles.sketchCanvas}
						strokeColor="black"
						strokeWidth={5}
						onStrokeStart={this.onStrokeStartHandler}
						onStrokeEnd={this.onStrokeEndHandler}
					/>
				</View>
				<Button
					title={isClearDisabled ? 'Try it' : 'Clear'}
					onPress={this.clear}
					containerStyle={styles.clearButtonContainer}
					disabled={isClearDisabled}
				/>
			</View>
		);
	}
}

export default SketchCanvasComponent;
