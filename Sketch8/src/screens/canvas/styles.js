import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	canvasContainer: {
		position: 'absolute',
		top: 0,
		left: 0,
		backgroundColor: 'white',
	},
	sketchCanvasContainer: {
		flex: 1,
		flexDirection: 'row',
	},
	sketchCanvas: {
		flex: 1,
		backgroundColor: 'transparent',
	},
	shapesContainer: {
		position: 'absolute',
		top: -40,
		left: 0,
		width: '100%',
	},
	clearButtonContainer: {
		position: 'absolute',
		bottom: 10,
		left: '42%',
	},
});
