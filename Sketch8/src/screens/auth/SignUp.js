/**
 * @format
 * @flow
 */

import React from 'react';
import { View, Text, KeyboardAvoidingView, AsyncStorage } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import type { NavigationScreenProp } from 'react-navigation';
import { Button } from 'react-native-elements';
import { Auth } from 'aws-amplify';
import uuidV4 from 'uuid/v4';

import { FormikInput, PasswordInput } from '../../components/inputs';
import commonStyles, { platformBehavior } from './commonStyles';
import styles from '../../styles';
import validation from './validation';

const validationSchema = Yup.object().shape({
	email: validation.email,
	password: validation.password,
});

type Props = { navigation: NavigationScreenProp<*> };
class SignUp extends React.Component<Props> {
	onSubmitHandler = async ({ email, password }: { email: string, password: string }) => {
		const { navigation } = this.props;
		const username = uuidV4();
		try {
			await Auth.signUp({ username, password, attributes: { email } });
			const authInfo = JSON.stringify({ username, password });
			await AsyncStorage.setItem('@sketch8:authInfo', authInfo);
			navigation.navigate('ConfirmCode');
		} catch (error) {
			console.log('Sign Up failure', error);
		}
	};

	render() {
		return (
			<View style={[styles.screenContainer, { padding: 20 }]}>
				<Text style={[iOSUIKit.title3Emphasized, { marginTop: 40, marginBottom: 20 }]}>
					Sign Up
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ email: '', password: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({ setFieldValue, values, setFieldTouched, errors, touched, handleSubmit }) => (
							<View style={commonStyles.formContainer}>
								<FormikInput
									placeholder="Email"
									containerStyle={commonStyles.inputContainer}
									name="email"
									onChangeText={setFieldValue}
									value={values.email}
									onBlur={setFieldTouched}
									errorMessage={errors.email && touched.email ? errors.email : ''}
									keyboardType="email-address"
								/>
								<PasswordInput
									setFieldValue={setFieldValue}
									value={values.password}
									setFieldTouched={setFieldTouched}
									error={errors.password}
									touched={touched.password}
								/>
								<Button
									title="Continue"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

export default SignUp;
