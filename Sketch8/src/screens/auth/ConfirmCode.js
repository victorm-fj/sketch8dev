/**
 * @format
 * @flow
 */

import React from 'react';
import { View, Text, KeyboardAvoidingView, AsyncStorage } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import type { NavigationScreenProp } from 'react-navigation';
import { Button } from 'react-native-elements';
import { Auth } from 'aws-amplify';
import { graphql } from 'react-apollo';

import { SaveMe } from '../../graphQL';
import { FormikInput } from '../../components/inputs';
import commonStyles, { platformBehavior } from './commonStyles';
import styles from '../../styles';
import validation from './validation';

const validationSchema = Yup.object().shape({
	code: validation.code,
});

type Props = { navigation: NavigationScreenProp<*>, onSaveMe: Function };
class ConfirmCode extends React.Component<Props> {
	onSubmitHandler = async ({ code }: { code: string }) => {
		console.log(code);
		const { navigation, onSaveMe } = this.props;
		try {
			const strAuthInfo = await AsyncStorage.getItem('@sketch8:authInfo');
			const { username, password } = JSON.parse(strAuthInfo);
			await Auth.confirmSignUp(username, code);
			await Auth.signIn(username, password);
			onSaveMe();
			await AsyncStorage.removeItem('@sketch8:authInfo');
			navigation.navigate('Today');
		} catch (error) {
			console.log('Confirm Code failure', error);
		}
	};

	render() {
		return (
			<View style={[styles.screenContainer, { padding: 20 }]}>
				<Text style={[iOSUIKit.title3Emphasized, { marginTop: 40, marginBottom: 20 }]}>
					Confirm your account
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ code: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({ setFieldValue, values, setFieldTouched, errors, touched, handleSubmit }) => (
							<View style={commonStyles.formContainer}>
								<FormikInput
									placeholder="Confirmation code"
									containerStyle={commonStyles.inputContainer}
									name="code"
									onChangeText={setFieldValue}
									value={values.code}
									onBlur={setFieldTouched}
									errorMessage={errors.code && touched.code ? errors.code : ''}
									keyboardType="numeric"
								/>
								<Button
									title="Confirm account"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

export default graphql(SaveMe, {
	props: ({ mutate }) => ({
		onSaveMe: () => mutate(),
	}),
})(ConfirmCode);
