import { StyleSheet, Platform } from 'react-native';
import { iOSColors } from 'react-native-typography';

export const platformBehavior = Platform.OS === 'ios' ? 'padding' : 'height';

export default StyleSheet.create({
	screenContainer: {
		flex: 1,
		alignItems: 'center',
		padding: 16,
	},
	rowContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: 16,
	},
	formContainer: {
		flex: 1,
		paddingTop: 16,
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	inputContainer: {
		width: 220,
		marginBottom: 24,
	},
	formButtonContainer: {
		marginBottom: 20,
	},
	footnoteContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		marginVertical: 16,
	},
	footnoteText: {
		padding: 0,
		paddingLeft: 10,
		fontWeight: 'bold',
		color: iOSColors.black,
	},
});
