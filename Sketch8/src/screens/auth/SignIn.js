/**
 * @format
 * @flow
 */

import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
import { Formik } from 'formik';
import * as Yup from 'yup';
import type { NavigationScreenProp } from 'react-navigation';
import { Button } from 'react-native-elements';
import { Auth } from 'aws-amplify';

import { FormikInput, PasswordInput } from '../../components/inputs';
import commonStyles, { platformBehavior } from './commonStyles';
import styles from '../../styles';
import validation from './validation';

const validationSchema = Yup.object().shape({
	email: validation.email,
	password: validation.password,
});

type Props = { navigation: NavigationScreenProp<*> };
class SignIn extends React.Component<Props> {
	onSubmitHandler = async ({ email, password }: { email: string, password: string }) => {
		console.log(email, password);
		const { navigation } = this.props;
		try {
			await Auth.signIn(email, password);
			navigation.navigate('Today');
		} catch (error) {
			console.log('Sign In failure', error);
		}
	};

	render() {
		return (
			<View style={[styles.screenContainer, { padding: 20 }]}>
				<Text style={[iOSUIKit.title3Emphasized, { marginTop: 40, marginBottom: 20 }]}>
					Sign In
				</Text>
				<KeyboardAvoidingView
					style={{ flex: 1 }}
					keyboardVerticalOffset={64}
					behavior={platformBehavior}
				>
					<Formik
						initialValues={{ email: '', password: '' }}
						validationSchema={validationSchema}
						onSubmit={this.onSubmitHandler}
						render={({ setFieldValue, values, setFieldTouched, errors, touched, handleSubmit }) => (
							<View style={commonStyles.formContainer}>
								<FormikInput
									placeholder="Email"
									containerStyle={commonStyles.inputContainer}
									name="email"
									onChangeText={setFieldValue}
									value={values.email}
									onBlur={setFieldTouched}
									errorMessage={errors.email && touched.email ? errors.email : ''}
									keyboardType="email-address"
								/>
								<PasswordInput
									setFieldValue={setFieldValue}
									value={values.password}
									setFieldTouched={setFieldTouched}
									error={errors.password}
									touched={touched.password}
								/>
								<Button
									title="Continue"
									onPress={handleSubmit}
									containerStyle={commonStyles.formButtonContainer}
									icon={{
										name: 'arrow-forward',
										size: 20,
										color: 'white',
									}}
									iconRight
								/>
							</View>
						)}
					/>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

export default SignIn;
