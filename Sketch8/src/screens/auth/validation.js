import * as Yup from 'yup';

export default {
	code: Yup.string()
		.matches(
			/[\d]{6}/,
			"This code isn't valid. Please copy and paste the code of the email you received."
		)
		.max(6, "This code isn't valid. Please copy and paste the code of the email you received.")
		.required('Code is required'),
	email: Yup.string()
		.email('Invalid email.')
		.required('Email is required.'),
	password: Yup.string()
		.matches(
			/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
			'Password must contain at least 8 characters, 1 number, 1 upper and 1 lowercase letter, and 1 special character'
			// 'Invalid password.'
		)
		.required('Password is required'),
	passwordConfirm: Yup.string()
		.oneOf([Yup.ref('password')], 'Passwords do not match')
		.required('Password confirm is required'),
};
