/**
 * @format
 * @flow
 */
import React from 'react';
import { View, Text, StatusBar, SectionList, AsyncStorage } from 'react-native';
import { iOSUIKit, iOSColors } from 'react-native-typography';
import { Icon } from 'react-native-elements';
import type { NavigationScreenProp } from 'react-navigation';
import { request } from 'graphql-request';
import moment from 'moment';

import Card from '../../components/card/Card';
import Loader from '../../components/Loader';
import styles from '../../styles';
import todayStyles from './todayStyles';

const SERVER_URL = 'http://zilln.com';

const compareDates = (a, b) => {
	const aDate = new Date(a.data[0].date).getTime();
	const bDate = new Date(b.data[0].date).getTime();
	if (aDate < bDate) {
		return 1;
	}
	if (aDate > bDate) {
		return -1;
	}
	return 0;
};

const getLastWeek = () => {
	const today = new Date();
	const lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
	return lastWeek.toISOString();
};

type Props = { navigation: NavigationScreenProp<*> };
type State = { challenges: Array<Object>, loading: boolean };
class Home extends React.Component<Props, State> {
	state = { challenges: [], loading: true };

	async componentDidMount() {
		try {
			const date = getLastWeek();
			const query = `{
				challenges(
					where: {
						date_gt: "${date}",
						release: true
					}
				) {
					_id
					title
					titleColor
					titlePosition
					actionTitle
					actionSubtitle
					actionTextColor
					actionBackgroundColor
					body
					image {
						_id
						url
					}
					date
					release
					coins
				}
			}`;
			const result = await request(`${SERVER_URL}/graphql`, query);
			console.log('fetchCards result:', result);
			const challenges = this.filterData(result.challenges);
			this.setState({ challenges, loading: false });
		} catch (error) {
			console.log('fetchCards error:', error);
			this.setState({ loading: false });
		}
	}

	onPressHandler = (challenge: Object) => {
		const { navigation } = this.props;
		navigation.navigate('Task', { challenge });
	};

	filterData = (data: Array<Object>) => {
		const titles = data.map(c =>
			moment(c.date)
				.format('dddd DD MMMM')
				.toUpperCase()
		);
		const sectionsObj = {};
		titles.forEach(title => {
			sectionsObj[title] = { title, data: [] };
		});
		data.forEach(challenge => {
			const cTitle = moment(challenge.date)
				.format('dddd DD MMMM')
				.toUpperCase();
			sectionsObj[cTitle].data.push(challenge);
		});
		return Object.values(sectionsObj).sort(compareDates);
	};

	onPressSolveHandler = async (coins: number) => {
		const { navigation } = this.props;
		await AsyncStorage.setItem('@challenge:coins', JSON.stringify(coins));
		navigation.navigate('ScreenOne');
	};

	renderItem = ({ item }) => {
		const {
			_id,
			title,
			titleColor,
			titlePosition,
			image,
			actionTitle,
			actionSubtitle,
			actionBackgroundColor,
			actionTextColor,
			coins,
		} = item;
		return (
			<Card
				key={_id}
				id={_id}
				onPress={() => this.onPressHandler(item)}
				image={{ uri: `${image.url}` }}
				imageText={title}
				imageTextColor={titleColor}
				imageTextPosition={titlePosition}
				footer={{
					title: actionTitle,
					subtitle: actionSubtitle,
					textColor: actionTextColor,
					backgroundColor: actionBackgroundColor,
				}}
				coins={coins}
				onPressSolve={() => this.onPressSolveHandler(coins)}
			/>
		);
	};

	renderSectionHeader = ({ section: { title } }) => {
		const isToday =
			moment()
				.format('dddd DD MMMM')
				.toUpperCase() === title;
		return (
			<View style={todayStyles.sectionHeaderContainer}>
				<View style={todayStyles.headerDateContainer}>
					<Text style={[iOSUIKit.footnoteEmphasized, { color: iOSColors.gray }]}>{title}</Text>
					{isToday ? <Text style={iOSUIKit.largeTitleEmphasized}>Today</Text> : null}
				</View>
				{isToday ? (
					<Icon
						type="ionicon"
						name="ios-contact"
						size={45}
						color={iOSColors.blue}
						containerStyle={todayStyles.headerIconContainer}
					/>
				) : null}
			</View>
		);
	};

	render() {
		const { loading, challenges } = this.state;
		if (loading) {
			return <Loader />;
		}

		return (
			<View style={styles.screenContainer}>
				<StatusBar hidden={false} />
				<SectionList
					style={todayStyles.scrollViewContainer}
					contentContainerStyle={todayStyles.scrollViewContent}
					sections={challenges}
					renderSectionHeader={this.renderSectionHeader}
					renderItem={this.renderItem}
					keyExtractor={item => item._id}
					stickySectionHeadersEnabled={false}
				/>
			</View>
		);
	}
}

export default Home;
