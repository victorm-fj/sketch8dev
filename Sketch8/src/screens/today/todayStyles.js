import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	scrollViewContainer: {
		flex: 1,
		width: '100%',
		marginTop: 20,
	},
	scrollViewContent: {
		flexGrow: 1,
		marginTop: -10,
		padding: 20,
		paddingBottom: 80,
	},
	sectionHeaderContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: 20,
	},
	headerDateContainer: {
		flex: 1,
	},
	headerIconContainer: {
		paddingTop: 10,
	},
});
