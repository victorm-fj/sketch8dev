/**
 * @format
 * @flow
 */

import React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import { Auth } from 'aws-amplify';
import type { NavigationScreenProp } from 'react-navigation';
import { iOSUIKit } from 'react-native-typography';

import styles from '../../styles';

type Props = { navigation: NavigationScreenProp<*> };
class Settings extends React.Component<Props> {
	onSignOutHandler = async () => {
		const { navigation } = this.props;
		try {
			await Auth.signOut();
			navigation.navigate('Intro');
		} catch (error) {
			console.log('Sign Out failure', error);
		}
	};

	render() {
		return (
			<View style={[styles.screenContainer, { padding: 20 }]}>
				<Text style={[iOSUIKit.title3Emphasized, { marginBottom: 20 }]}>Settings screen</Text>
				<Button title="Sign Out" onPress={this.onSignOutHandler} />
			</View>
		);
	}
}

export default Settings;
