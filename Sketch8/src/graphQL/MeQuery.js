import gql from 'graphql-tag';

export default gql`
	query Me {
		me {
			__typename
			userId
			coins
		}
	}
`;
