import gql from 'graphql-tag';

export default gql`
	mutation SaveMe {
		saveMe {
			__typename
			userId
			coins
		}
	}
`;
