import gql from 'graphql-tag';

export default gql`
	mutation UpdateMe($info: UserInput!) {
		updateMe(info: $info) {
			__typename
			userId
			coins
		}
	}
`;
