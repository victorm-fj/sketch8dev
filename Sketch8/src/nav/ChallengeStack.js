import React from 'react';
import { View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import StepIndicator from 'react-native-step-indicator';
import { Button } from 'react-native-elements';

import ScreenOne from '../screens/challenge/ScreenOne';
import ScreenTwo from '../screens/challenge/ScreenTwo';
import ScreenThree from '../screens/challenge/ScreenThree';
import styles, { stepIndicatorStyles } from './styles';

const navigationOptions = ({ navigation }) => {
	const {
		state: { routeName },
	} = navigation;
	let index = 0;
	let headerLeftAction = () => navigation.goBack();
	let headerRightAction = () => navigation.navigate('ScreenTwo');
	if (routeName === 'ScreenOne') {
		headerLeftAction = () => navigation.navigate('Today');
	}
	if (routeName === 'ScreenTwo') {
		index = 1;
		headerRightAction = () => navigation.navigate('ScreenThree');
	}
	if (routeName === 'ScreenThree') {
		index = 2;
		headerRightAction = () => navigation.navigate('Today');
	}
	const headerLeft = (
		<Button title="Back" titleStyle={styles.buttonTitleStyle} onPress={headerLeftAction} clear />
	);
	const headerTitle = (
		<View style={styles.stepIndicatorContainer}>
			<StepIndicator currentPosition={index} customStyles={stepIndicatorStyles} stepCount={3} />
		</View>
	);
	const headerRight = (
		<Button
			title="Continue"
			titleStyle={styles.buttonTitleStyle}
			onPress={headerRightAction}
			clear
		/>
	);
	return { headerLeft, headerTitle, headerRight };
};

export default createStackNavigator(
	{
		ScreenOne: { screen: ScreenOne },
		ScreenTwo: { screen: ScreenTwo },
		ScreenThree: { screen: ScreenThree },
	},
	{
		initialRouteName: 'ScreenOne',
		navigationOptions,
	}
);
