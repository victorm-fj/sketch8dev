import { createStackNavigator } from 'react-navigation';

import Canvas from '../screens/canvas/SketchCanvas';
import HomeNav from './HomeNav';
import ChallengeStack from './ChallengeStack';

export default createStackNavigator(
	{
		Home: { screen: HomeNav, navigationOptions: { header: null } },
		Challenge: { screen: ChallengeStack, navigationOptions: { header: null } },
		Canvas: { screen: Canvas, navigationOptions: { header: null } },
	},
	{
		mode: 'modal',
		headerMode: 'none',
	}
);
