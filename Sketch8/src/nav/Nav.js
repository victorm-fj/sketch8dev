import { createSwitchNavigator } from 'react-navigation';

import AuthLoadingScreen from './AuthLoadingScreen';
import AuthNav from './AuthNav';
import AppNav from './AppNav';

export default createSwitchNavigator(
	{
		AuthLoading: AuthLoadingScreen,
		Auth: AuthNav,
		App: AppNav,
	},
	{
		initialRouteName: 'AuthLoading',
	}
);
