/**
 * @format
 * @flow
 */

import React from 'react';
import { Auth } from 'aws-amplify';
import type { NavigationScreenProp } from 'react-navigation';

import Loader from '../components/Loader';

type Props = { navigation: NavigationScreenProp<*> };
class AuthLoadingScreen extends React.Component<Props> {
	async componentDidMount() {
		const { navigation } = this.props;
		try {
			await Auth.currentAuthenticatedUser();
			navigation.navigate('App');
		} catch (error) {
			console.log('auth user failure', error);
			navigation.navigate('Auth');
		}
	}

	render() {
		return <Loader />;
	}
}

export default AuthLoadingScreen;
