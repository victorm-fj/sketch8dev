import { createFluidNavigator } from 'react-navigation-fluid-transitions';

import Shop from '../screens/shop/Shop';
import Product from '../screens/product/Product';
import ProductGallery from '../screens/productGallery/ProductGallery';

const ShopFluidNav = createFluidNavigator(
	{
		Shop: { screen: Shop },
		Product: { screen: Product },
		ProductGallery: { screen: ProductGallery },
	},
	{
		navigationOptions: { gesturesEnabled: true },
	}
);

ShopFluidNav.navigationOptions = ({ navigation }) => ({
	tabBarVisible: navigation.state.index === 0,
});

export default ShopFluidNav;
