/**
 * @format
 * @flow
 */

import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import { iOSColors } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

import TodayFluidNav from './TodayFluidNav';
import ShopFluidNav from './ShopFluidNav';
import Wallet from '../screens/wallet/Wallet';
import Settings from '../screens/settings/Settings';

const getIconByName = (name: string): string => {
	if (Platform.OS === 'ios') {
		return `ios-${name}`;
	}
	return `md-${name}`;
};

export default createBottomTabNavigator(
	{
		Today: { screen: TodayFluidNav },
		Shop: { screen: ShopFluidNav },
		Wallet: { screen: Wallet },
		Settings: { screen: Settings },
	},
	{
		navigationOptions: ({ navigation }) => {
			const tabBarIcon = ({ tintColor }: { tintColor: string }) => {
				const { routeName } = navigation.state;
				let iconName;
				switch (routeName) {
					case 'Today':
						iconName = getIconByName('today');
						break;
					case 'Shop':
						iconName = getIconByName('pricetags');
						break;
					case 'Wallet':
						iconName = getIconByName('wallet');
						break;
					case 'Settings':
						iconName = getIconByName('settings');
						break;
					default:
						iconName = getIconByName('today');
						break;
				}
				return <Ionicons name={iconName} size={26} color={tintColor} />;
			};
			return { tabBarIcon };
		},
		tabBarOptions: {
			activeTintColor: iOSColors.blue,
			inactiveTintColor: iOSColors.gray,
			style: {
				backgroundColor: 'rgba(238,238,238,0.95)',
				position: 'absolute',
				left: 0,
				right: 0,
				bottom: 0,
			},
		},
	}
);
