import { createStackNavigator } from 'react-navigation';

import Intro from '../screens/intro/Intro';
import SignIn from '../screens/auth/SignIn';
import SignUp from '../screens/auth/SignUp';
import ConfirmCode from '../screens/auth/ConfirmCode';

export default createStackNavigator(
	{
		Intro: { screen: Intro, navigationOptions: { header: null } },
		SignIn: { screen: SignIn },
		SignUp: { screen: SignUp },
		ConfirmCode: { screen: ConfirmCode },
	},
	{
		mode: 'modal',
		headerMode: 'none',
	}
);
