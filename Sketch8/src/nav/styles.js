import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	headerLeftContainer: {
		paddingHorizontal: 15,
	},
	headerLeftIcon: {
		height: 44,
		width: 18,
	},
	titleContainerStyle: {
		paddingTop: 5,
		justifyContent: 'center',
		alignItems: 'center',
	},
	titleTextStyle: {
		fontFamily: 'GothamRounded-Book',
		letterSpacing: 3,
		fontSize: 18,
	},
	headerContainerStyle: {
		backgroundColor: '#f9fafc',
		borderBottomWidth: 0,
		shadowColor: '#3b4859',
		shadowOpacity: 0.35,
		shadowOffset: { height: 0.5 },
		shadowRadius: 1.5,
		elevation: 4,
	},
	buttonTitleStyle: {
		color: '#3b7cff',
	},
	stepIndicatorContainer: {
		width: 100,
		height: 48,
		justifyContent: 'center',
	},
});

export const stepIndicatorStyles = {
	stepIndicatorSize: 20,
	currentStepIndicatorSize: 25,
	separatorStrokeWidth: 2,
	currentStepStrokeWidth: 3,
	stepStrokeCurrentColor: '#3b7cff',
	stepStrokeWidth: 3,
	stepStrokeFinishedColor: '#3b7cff',
	stepStrokeUnFinishedColor: '#aaaaaa',
	separatorFinishedColor: '#3b7cff',
	separatorUnFinishedColor: '#aaaaaa',
	stepIndicatorFinishedColor: '#3b7cff',
	stepIndicatorUnFinishedColor: '#ffffff',
	stepIndicatorCurrentColor: '#ffffff',
	stepIndicatorLabelFontSize: 12,
	currentStepIndicatorLabelFontSize: 12,
	stepIndicatorLabelCurrentColor: '#3b7cff',
	stepIndicatorLabelFinishedColor: '#ffffff',
	stepIndicatorLabelUnFinishedColor: '#aaaaaa',
	labelColor: '#999999',
	labelSize: 12,
	currentStepLabelColor: '#3b7cff',
};
