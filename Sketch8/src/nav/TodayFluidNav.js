import { createFluidNavigator } from 'react-navigation-fluid-transitions';

import Today from '../screens/today/Today';
import Task from '../screens/task/Task';

const TodayFluidNav = createFluidNavigator(
	{
		Today: { screen: Today },
		Task: { screen: Task },
	},
	{
		navigationOptions: { gesturesEnabled: true },
	}
);

TodayFluidNav.navigationOptions = ({ navigation }) => ({
	tabBarVisible: navigation.state.index === 0,
});

export default TodayFluidNav;
