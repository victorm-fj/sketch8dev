/**
 * @format
 * @flow
 */

import React from 'react';
import Amplify from 'aws-amplify';
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';
import SplashScreen from 'react-native-splash-screen';

import awsmobile from './aws-exports';
import client from './client';
import Nav from './nav/Nav';

Amplify.configure(awsmobile);

type Props = {};
class App extends React.Component<Props> {
	async componentDidMount() {
		client.initQueryManager();
		await client.resetStore();
		SplashScreen.hide();
	}

	render() {
		return (
			<ApolloProvider client={client}>
				<Rehydrated>
					<Nav />
				</Rehydrated>
			</ApolloProvider>
		);
	}
}

export default App;
