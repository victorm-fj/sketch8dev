import { StyleSheet } from 'react-native';
import { iOSColors } from 'react-native-typography';

const colors = {
	white: '#f9fafc',
	black: '#3b4859',
	gray: '#8392a7',
	purple: iOSColors.purple,
};

export default StyleSheet.create({
	screenContainer: {
		flex: 1,
		width: '100%',
		alignItems: 'center',
		backgroundColor: colors.white,
	},
});
